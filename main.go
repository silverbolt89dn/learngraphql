package main

import (
    "net/http"
    "github.com/chris-ramon/graphql-go/types"
    "github.com/sogko/graphql-go-handler"
    "fmt"
    "io/ioutil"
    "encoding/json"
)

var data map[string]User


var userType = types.NewGraphQLObjectType(
    types.GraphQLObjectTypeConfig{
        Name: "User",
        Fields: types.GraphQLFieldConfigMap{
            "id": &types.GraphQLFieldConfig{
              Type: types.GraphQLString,
            },
            "name":  &types.GraphQLFieldConfig{
                Type: types.GraphQLString,
            },
        },
    },
)

var queryType = types.NewGraphQLObjectType(types.GraphQLObjectTypeConfig{
    Name: "Query",
    Fields: types.GraphQLFieldConfigMap{
        "user": &types.GraphQLFieldConfig{
            Type: userType,
            Args: types.GraphQLFieldConfigArgumentMap{
                "id": &types.GraphQLArgumentConfig{
                    Type: types.GraphQLString,
                },
            },
            Resolve: func(p types.GQLFRParams) interface{} {
                idQuery, isOK  := p.Args["id"].(string)
                if isOK {
                    fmt.Println("id query ",idQuery, "data ",data[idQuery])
                    return data[idQuery]
                }else {
                    fmt.Println("loi ko lay duoc tham so")
                    return nil
                }
            },
        },
    },
})

var schema, _ = types.NewGraphQLSchema(
    types.GraphQLSchemaConfig{
        Query: queryType,
    },
)

//Test with Post
//curl -XPOST http://localhost:8080/graphql -H 'Content-Type: application/graphql' -d 'query Root{ user(id:"1"){name}  }'
//Test with Get
//http://localhost:3000/graphql?query={user(id:%221%22){name}}


type User struct {
    Id string `json:"id"`
    Name string `json:"name"`
}

func importJsonDataFromFile(fileName string,result interface{}) (isOK bool){
    content, err := ioutil.ReadFile(fileName)
    if err!=nil{
        fmt.Print("Error:",err)
        isOK = false
    }
    err=json.Unmarshal(content, result)
    if err!=nil{
        isOK = false
        fmt.Print("Error:",err)
    }
    isOK = true
    return
}


func main() {
    _ = importJsonDataFromFile("data.json",&data)
    fmt.Println(data)

    // create a graphl-go HTTP handler with our previously defined schema
    // and we also set it to return pretty JSON output
    h := gqlhandler.New(&gqlhandler.Config{
        Schema: &schema,
        Pretty: true,
    })

    // serve a GraphQL endpoint at `/graphql`
    http.Handle("/graphql", h)

    // and serve!
    http.ListenAndServe(":8080", nil)
}