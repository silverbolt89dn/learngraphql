/**
 * Created by thanhliem on 10/12/15.
 */

var graphql = require('graphql');

var queryType = new graphql.GraphQLObjectType({
    name: 'Query',
    fields: {
        latestPost: {
            type: graphql.GraphQLString,
            resolve: function () {
                return 'Hello World!';
            }
        }
    }
});

var schema = new graphql.GraphQLSchema({
    query: queryType
});